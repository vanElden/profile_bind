Puppet::Parser::Functions::newfunction(:get_zone_slave_config, :type => :rvalue, :doc => <<-EOS
Creates Hash for Bind Slave Config from Array
EOS
) do |argv|
  zones = argv[0]

  unless zones.is_a?(Array)
    raise(Puppet::ParseError, 'get_zone_slave_config(): Requires zones array')
  end

  result = {}

  zones.each do |zone|
    masters = 'mymasters;'
    if zone.is_a?(Hash)
      zname = zone.keys()[0]
      if zone.has_key?('masters')
        zmasters = zone['masters']
        unless zmasters.is_a?(Array)
          raise(Puppet::ParseError, 'get_zone_slave_config(): Slave master must come as array')
        end
        masters = ''
        zmasters.each do |zm|
          masters << zm
          masters << '; '
        end
        masters = masters.strip
      end
    else
      zname = zone
    end
    czone = Array[]
    czone.push('type slave')
    czone.push("file \"slaves/#{zname}.zone\"")
    czone.push("masters { #{masters} }")
    result[zname] = czone
  end

  return result
end
