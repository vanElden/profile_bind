Puppet::Parser::Functions::newfunction(:get_zone_master_config, :type => :rvalue, :doc => <<-EOS
Creates Hash for Bind Master Config from Array
EOS
) do |argv|
  zones = argv[0]

  unless zones.is_a?(Array)
    raise(Puppet::ParseError, 'get_zone_master_config(): Requires zones array')
  end

  result = {}

  zones.each do |zone|
    add_opts = []
    if zone.is_a?(Hash)
      zname = zone.keys()[0]
      add_opts = zone.values()[0]
      unless add_opts.is_a?(Array)
        raise(Puppet::ParseError, 'get_zone_master_config(): Additional options must be arrays')
      end
    else
      zname = zone
    end
    czone = Array[]
    czone.push('type master')
    czone.push("file \"data/#{zname}.hosts.signed\"")
    unless add_opts.empty?
      add_opts.each do |opt|
        czone.push("#{opt}")
      end
    end
    result[zname] = czone
  end

  return result
end
