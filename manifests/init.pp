class profile_bind (
) {
  include bind
  ::profile_hostfirewall::rule { '010 dns profile_bind':                 
    rule => {                                                               
      dport => 53,
      proto => [ 'tcp', 'udp' ],
    }
  }
}
