class profile_bind::master (
  Array $slaves = [],
  Array[Hash] $slave_zones = [],
  Array $transfer_allowed = [],
) {

  include ::profile_bind

  $hiera_zones = hiera_array('profile_bind::zones', [])

  $_master_zones = get_zone_master_config($hiera_zones)
  $_slave_zones = get_zone_slave_config($slave_zones)
  $_zones = merge($_master_zones, $_slave_zones)

  $_slave_acl = {
    'slaves' => $slaves,
  }
  $_transfer_acl = {
    'transfer_only' => $transfer_allowed
  }
  $_acls = merge($_slave_acl, $_transfer_acl)

  bind::server::conf { '/etc/named.conf':
    acls                   => $_acls,
    allow_transfer         => [ 'slaves', 'transfer_only' ],
    allow_query            => [ 'any' ],
    listen_on_addr         => [ 'any' ],
    listen_on_v6_addr      => [ 'any' ],
    managed_keys_directory => '/var/named/dynamic',
    recursion              => false,
    zones                  => $_zones,
  }
}
