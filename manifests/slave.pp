class profile_bind::slave (
  Optional[Array] $masters = undef,
) {

  include ::profile_bind

  $hiera_zones = hiera_array('profile_bind::zones', [])

  $_zones = get_zone_slave_config($hiera_zones)

  bind::server::conf { '/etc/named.conf':
    allow_query            => [ 'any' ],
    listen_on_addr         => [ 'any' ],
    listen_on_v6_addr      => [ 'any' ],
    managed_keys_directory => '/var/named/dynamic',
    masters                => {
      'mymasters' => $masters,
    },
    recursion              => false,
    zones                  => $_zones,
  }
}
